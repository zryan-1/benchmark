# Vector Benchmarking
 
Most of the code here is taken from [PyTorch Benchmark](https://github.com/pytorch/benchmark) with some modifications. This benchmark runs a subset of models of the PyTorch benchmark with some additions, namely Seq2Seq, MLP and GAT which we hope to contribute upstream later on.
 
All benchmarks run on cuda-eager which we believe is most indicative of the workloads of our cluster.
 
## Python Enviornment Installation
The python virtual environment supports python 3.7 in our suite. With the environment being installed using python [venv](https://docs.python.org/3.7/library/venv.html)
 
## Install the packages with cuda version dependencies
```
# create the venv via python3.7 -m venv env_to_use and then activate it
 
pip3 install torch==1.10.1+cu113 torchvision==0.11.2+cu113 torchtext -f https://download.pytorch.org/whl/cu113/torch_stable.html
pip3 install torch-scatter -f https://data.pyg.org/whl/torch-1.10.1+cu113.html
pip3 install torch-sparse -f https://data.pyg.org/whl/torch-1.10.1+cu113.html
```
 
Install the benchmark suite dependencies.  Currently, the repo is intended to be installed from the source tree.
```
git clone <benchmark>
cd <benchmark>
pip install -r requirements.txt
```
 
## Singularity Installation
 
For this fork, we have included the option to build a Singularity sandbox for benchmarking on a cluster. We have included instructions for building specifically on NSL-B. If you want to run Singularity locally, first you must [install Singularity](https://sylabs.io/guides/3.5/user-guide/quick_start.html).
 
Get NSL-B A100 GPU instance:
```
srun -p epyc_a100x4 --exclusive -G 1 --pty bash
```
 
On local machine/NSL-B compute node:
```
git clone https://gitlab.com/zryan-1/benchmark
cd ~/benchmark
singularity build --fakeroot sandbox.sif Singularity
```
 
On the system where you are trying to deploy your Singularity container, convert the `sandbox.sif` file to a `sandbox` workspace so you can write out results and make changes where necessary during benchmarking. Example is shown on NSL-B.
 
```
singularity build --fakeroot --fix-perms --sandbox vector_sandbox sandbox.sif
```
 
Running Singularity sandbox from your home directory on NSL-B. There is an additional step needed to create an empty directory wherever you cloned the repo.
 
```
mkdir torch_overlay
singularity shell --fakeroot --nv --overlay torch_overlay/ vector_sandbox/
```
 
## Running our benchmark
 
```
cd benchmark
bash run_bench.sh 0
```
 
This script will then produce .out, .csv, .json files which can be shared.
